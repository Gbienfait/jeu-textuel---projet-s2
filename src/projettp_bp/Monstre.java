/*
 * Copyright (C) 2018 Guillaume & Eloi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package projettp_bp;

import java.util.ArrayList;

/**
 *
 * @author eac, gbienfait
 *
 * Classe Monstre qui hérite de la classe abstraite PNJ.
 */
public class Monstre extends PNJ {

    double attaque;
    double defense;
    double vie;
    boolean mort = false;

    /// Tableaux des valeurs des énumérations associées, ils permettent par la suite de séléctionner aléatoirement une des valeurs
    private final ConsoConcret[] cC = ConsoConcret.values();
    private final EquipementConcret[] eC = EquipementConcret.values();

    /**
     * Constructeur d'un monstre. Tout comme nous, il aura certaines
     * caractéristiques semblables
     *
     * @param nom, un monstre aura une appélation propre.
     * @param vie, nombre de points de vie d'un monstre.
     * @param attaque, stats d'attauqe d'un monstre.
     * @param defense, stats de défense d'un monstre.
     */
    public Monstre(String nom, double vie, double attaque, double defense) {
        super(nom);
        this.vie = vie;
        this.attaque = attaque;
        this.defense = defense;
    }

    /**
     * Change l'état d'un monstre à mort
     */
    public void abattu() {
        mort = true;
    }

    /**
     * Méthode utiliser à la mort du monstre pour que le joueur récupère une récompense
     * 
     * @return une liste d'objet
     */
    public ArrayList<Objet> getButin() {
        ArrayList<Objet> butin = new ArrayList<>();
        int rd = (int) (Math.random() * 4); // Un nombre aléatoire entre 0 et 3
        int rd1 = (int) (Math.random() * cC.length);
        int rd2 = (int) (Math.random() * eC.length);
        
        switch (rd) {
            case 0:
                // Le monstre donne un Consommable à sa mort.
                butin.add(ConsoConcret.getConso(cC[rd1].toString()));
                break;
            case 1:
                // Le monstre donne un Equipement à sa mort.
                butin.add(EquipementConcret.getEquipement(eC[rd2].toString()));
                break;  
            case 2:
                // Le monstre donne un Consommable et un Equipement à sa mort.
                butin.add(ConsoConcret.getConso(cC[rd1].toString()));
                butin.add(EquipementConcret.getEquipement(eC[rd2].toString()));
                break;
            default:
                // Le monstre ne donne aucun objets à sa mort.
                break;
        }
        return butin;
    }
}
