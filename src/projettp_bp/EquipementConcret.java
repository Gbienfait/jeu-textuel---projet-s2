/*
 * Copyright (C) 2018 Guillaume & Eloi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package projettp_bp;

/**
 *
 * @author eac, gbienfait
 *
 * Enumération des équipements concrets, que l'on peut équiper à notre joueur.
 */
public enum EquipementConcret {
    BOTTESENCUIR(0, 5, TypeEquipement.BOTTES, "Bottes de cuir", 4),
    JEANUSE(0, 7, TypeEquipement.JAMBIERES, "Jean usé", 5),
    ARMURELEGERE(0, 15, TypeEquipement.PLASTRON, "Armure légère", 10),
    CASQUETTENY(0, 5, TypeEquipement.CASQUE, "Casquette NY", 5),
    JETONROUGE(0, 0, TypeEquipement.OBJETDEQUETE, "Un jeton rouge écarlate", 50),
    JETONVERT(0, 0, TypeEquipement.OBJETDEQUETE, "Un jeton vert émeraude", 50),
    JETONBLEU(0, 0, TypeEquipement.OBJETDEQUETE, "Un jeton bleu cobalt", 50);
    
    private final double defense;
    private final double attaque;
    private final TypeEquipement typeEquip;
    private final String nom;
    private final int prix;

    /**
     * Constructeur d'un équipement concret.
     *
     * @param attaque, correspond à un entier qui établira l'amélioration de
     *                  l'attaque.
     * @param defense, correspond à un entier qui établira l'amélioration de la
     *                  défense.
     * @param typeEquip, un objet sera d'un type précis (casque, plastron, etc.)
     * @param nom, un objet a un nom qui lui est propre.
     * @param prix, on associera un prix à un objet précis.
     */
    private EquipementConcret(double attaque, double defense, TypeEquipement typeEquip, String nom, int prix) {
        this.defense = defense;
        this.attaque = attaque;
        this.typeEquip = typeEquip;
        this.nom = nom;
        this.prix = prix;
    }
    
    /**
     * Permet d'obtenir un Equipement à partir de son nom.
     *
     * @param nom
     * @return un Equipement à partir de son nom.
     */
    public static Equipement getEquipement(String nom){
        for(EquipementConcret eC : EquipementConcret.values()){
            if(eC.toString().equals(nom)){
                return (new Equipement(eC.attaque, eC.defense, eC.typeEquip, eC.nom, eC.prix));
            }
        }
        return null;
    }
    
    @Override
    public String toString(){
        return nom;
    }
}
