/*
 * Copyright (C) 2018 Guillaume & Eloi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package projettp_bp;

/**
 *
 * @author eac, gbienfait
 *
 * Classe abstraite Objet.
 */
public abstract class Objet {
    String nom;
    int prix;

    /**
     * Contructeur d'un Objet.
     *
     * @param nom, un objet a un nom qui lui est propre.
     * @param prix, on associera un prix à un objet précis.
     */
    public Objet(String nom, int prix) {
        this.nom = nom;
        this.prix = prix;
    }

    /**
     * Getter du nom de l'objet.
     *
     * @return nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Getter du prix de l'objet.
     *
     * @return prix
     */
    public int getPrix() {
        return prix;
    }
    
    /**
     * Par défaut un objet n'est pas un objet de quête.
     *
     * @return false
     */
    public boolean EstObjetDeQuete(){
        return false;
    }
    
    /**
     * Par défaut un objet n'est pas un Equipement
     *
     * @return false
     */
    public boolean EstEquipement(){
        return false;
    }
}
