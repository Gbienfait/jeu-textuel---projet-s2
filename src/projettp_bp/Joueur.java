/*
 * Copyright (C) 2018 Guillaume & Eloi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package projettp_bp;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 *
 * @author eac, gbienfait
 */
public class Joueur {

    /// Ici figurent les différents attributs propres au joueur que l'on contrôle ///
    private double vie = 100;
    private double vieMax = 100;
    private int argent = 0;
    private ArrayList<Objet> inventaire = new ArrayList<>();
    private EnumMap<TypeEquipement, Equipement> equipementsPortés = new EnumMap<>(TypeEquipement.class);
    private double attaque = 10;
    private double defense = 10;
    private boolean enAgonie = false;

    public Joueur() {
    }

    /**
     * Getter de l'inventaire du joueur, consultable tout au long du jeu.
     *
     * @return inventaire
     */
    public ArrayList<Objet> getInventaire() {
        return inventaire;
    }

    /**
     * Affiche le monstre et les objets présents dans la salle
     * 
     * @param salle, la salle actuel
     */
    public void inspecter(Salle salle) {
        salle.getObjetsPrésents().forEach(n -> System.out.println("\033[34m" + n.getNom() + "\033[00m"));
        if (salle.getMonstrePrésent() != null) {
            if (salle.getMonstrePrésent().mort){
                System.out.println(salle.getMonstrePrésent().nom + "(cadavre)");
            } else {
                System.out.println("\033[31m" + salle.getMonstrePrésent().nom + "\033[00m");
            }
        }
    }

    /**
     * Le joueur attaque un monstre, gère toutes les conséquences de l'attaque dont la mort du joueur
     * 
     * @param MAtt, le monstre présent dans la salle
     */
    public void attaquer(Monstre MAtt) {
        if (!MAtt.mort) {

            /// Phase combat ///
            double degatPris1Attaque = MAtt.attaque * ((100 - defense) / 100);
            // = Dégat reçu du monstre en une attaque de celui-ci, une défense de 10 protège de 10% de l'attaque du monstre.
            double nbAttaqueNécessaire = MAtt.vie / (attaque * ((100 - MAtt.defense) / 100));
            // = Nombre d'attaque nécessaire de la part du joueur pour abattre le monstre.
            double degatPrisTotal = degatPris1Attaque * nbAttaqueNécessaire;
            // On considère ici que pour une attaque qui atteint le monstre, le monstre en inflige une en retour.
            vie -= degatPrisTotal;

            /// Phase conséquences combat ///
            if (vie <= 0 && !enAgonie) {
                enAgonie = true;
                vie = 1;
                attaque /= 2;
                defense /= 2;
                System.out.println(MAtt.nom + " retourne à la terre mais vous n'êtes pas loin de le rejoindre...");
                System.out.println("Vous êtes à l'agonie, vos points de vie sont réduits à 1 et vos stats sont réduites de moitié");
            } else if (vie <= 0 && enAgonie) {
                System.out.println("C'était le combat de trop, vous périssez sous les coups de " + MAtt.nom + " .");
                System.exit(0);
            }
            System.out.println("Le combat fut rude, il vous reste " + "\033[33m" + vie + "\033[00m" + " points de vie.");

            /// Phase récupération du butins(=drop) du monstre) ///
            ArrayList<Objet> butin = MAtt.getButin();
            if (!butin.isEmpty()) {
                inventaire.addAll(butin);
                System.out.println("Vous récupérez sur la dépouille : ");
                butin.forEach(n -> System.out.println("\033[34m" + n.getNom() + "\033[00m"));
                verifTroisJetonsCollecté();
            }

        } else {
            System.out.println("Cette chose est déjà sans vie.");
        }
    }

    /**
     * Propose au joueur de ramasser un des objets de la salle à l'aide d'une sélection par association chiffre-objet
     * 
     * @param salle, la salle actuel
     */
    public void ramasser(Salle salle) {
        int entrée = -1;
        for (int i = 0; i < salle.getObjetsPrésents().size(); i++) {
            System.out.println("\033[00m" + i + ". " + "\033[34m" + salle.getObjetsPrésents().get(i).getNom() + "\033[00m");
        }
        do {
            System.out.print("Taper le numéro de l'objet que vous voulez ramasser : ");
            try {
                Scanner in = new Scanner(System.in);
                Scanner s = new Scanner(in.nextLine());
                entrée = Integer.parseInt(s.next());
            } catch (NoSuchElementException e) {
                System.out.println("Aucun chiffre rentré");
            } catch (NumberFormatException e) {
                System.out.println("Le caractère entré n'est pas correct");
            }
            if ((entrée < 0 || entrée >= salle.getObjetsPrésents().size())) {
                System.out.println("Veuillez rentrer un des chiffres affichés.");
            }
        } while ((entrée < 0 || entrée >= salle.getObjetsPrésents().size()));
        inventaire.add(salle.getObjetsPrésents().get(entrée));
        System.out.println("\033[00m" + "L'objet " + "\033[34m" + salle.getObjetsPrésents().get(entrée).getNom() + "\033[00m" + " a été ajouté à votre inventaire.");
        salle.getObjetsPrésents().remove(salle.getObjetsPrésents().get(entrée));
        verifTroisJetonsCollecté();
    }

    /**
     * Affiche tous les objets dans l'inventaire du joueur
     */
    public void voirInventaire() {
        if (!inventaire.isEmpty()) {
            for (Objet objet : inventaire) {
                System.out.println(objet.toString());
            }
        } else {
            System.out.println("\033[00m" + "Inventaire vide" + "\n");
        }
    }

    /**
     * Propose au joueur d'utiliser un des objets de son inventaire à l'aide d'une sélection par association chiffre-objet,
     * gère les choix sur un équipement ainsi que sur un consommable
     */
    public void utiliser() {
        if (!inventaire.isEmpty()) {
            int entrée = -1;
            for (int i = 0; i < inventaire.size(); i++) {
                System.out.println("\033[00m" + i + ". " + "\033[34m" + inventaire.get(i).getNom() + "\033[00m");
            }
            do {
                System.out.print("Taper le numéro de l'objet que vous voulez utiliser : ");
                try {
                    Scanner in = new Scanner(System.in);
                    Scanner s = new Scanner(in.nextLine());
                    entrée = Integer.parseInt(s.next());
                } catch (NoSuchElementException e) {
                    System.out.println("Aucun chiffre rentré");
                } catch (NumberFormatException e) {
                    System.out.println("Le caractère entré n'est pas correct");
                }
                if ((entrée < 0 || entrée >= inventaire.size())) {
                    System.out.println("Veuillez rentrer un des chiffres affichés.");
                }
            } while ((entrée < 0 || entrée >= inventaire.size()));
            if (inventaire.get(entrée).getClass() == Consommable.class) {
                consommer((Consommable) inventaire.get(entrée));
            } else {
                if (inventaire.get(entrée).EstObjetDeQuete()) {
                    System.out.println("Vous ne pouvez pas utilisez un objet de quête.");
                } else {
                    equiper((Equipement) inventaire.get(entrée));
                }
            }
        } else {
            System.out.println("Votre inventaire est vide.");
        }
    }

    /**
     * Affiche les caractéristiques actuelles du joueur ainsi que ses équipements portés
     */
    void afficheStatut() {
        if (enAgonie) {
            System.out.println("\033[31m" + "   AGONIE " + "\033[00m");
        }
        System.out.println("    vie : " + vie + "\n"
                + "    attaque : " + attaque + "\n"
                + "    defense : " + defense);

        for (TypeEquipement te : TypeEquipement.values()) {
            if (te != TypeEquipement.OBJETDEQUETE) {
                if (equipementsPortés.get(te) == null) {
                    System.out.println("    " + te + " : ");
                } else {
                    System.out.println("    " + te + " : " + equipementsPortés.get(te).nom);
                }
            }
        }
    }

    /**
     * Consomme un objet Consommable, retire l'objet de l'inventaire.
     * 
     * @param conso, le consommable sélectionné pour restaurer votre vie 
     */
    private void consommer(Consommable conso) {
        vie += conso.soins;
        if (vie > vieMax) {
            vie = vieMax;
        }
        inventaire.remove(conso);
        System.out.println("Votre vie est maintenant de " + "\033[33m" + vie + "\033[00m");
    }

    /**
     * Equipe le joueur d'un objet Equipement, retire l'objet de l'inventaire.
     * 
     * @param equipement, l'équipement sélectionné pour être porté
     */
    private void equiper(Equipement equipement) {
        if (equipementsPortés.containsValue(equipement)) {
            desequiper(equipementsPortés.get(equipement.getType()));
        }
        equipementsPortés.put(equipement.getType(), equipement);
        System.out.println("L'objet " + "\033[34m" + equipement.getNom() + "\033[00m" + " a été équipé" + "\033[00m");
        inventaire.remove(equipement);
        attaque = 10 + equipement.getAttaque();
        defense = 10 + equipement.getDefense();
    }

    /**
     * Déséquipe le joueur d'un objet Equipement, ajoute l'objet dans l'inventaire.
     * 
     * @param equipement, l'équipement sélectionné pour être déséquipé
     */
    private void desequiper(Equipement equipement) {
        equipementsPortés.remove(equipement.getType());
        inventaire.add(equipement);
        System.out.println("\033[34m" + equipement.getNom() + "\033[00m" + " a été déséquipé.");
    }

    /**
     * Vérifie si les trois jetons de couleurs sont dans l'inventaire
     */
    private void verifTroisJetonsCollecté() {
        
        // Création d'une collection avec seulement les équipements stockés dans l'inventaire.
        ArrayList<Equipement> equipementsInv = new ArrayList<>();
        for (Objet obj : inventaire) {
            if (obj.EstEquipement()) {
                equipementsInv.add((Equipement) obj);
            }
        }
        
        // Est-ce que les équipements stockés dans l'inventaire contiennent les trois différents jetons ?
        if (equipementsInv.contains(EquipementConcret.getEquipement(EquipementConcret.JETONBLEU.toString()))
                && equipementsInv.contains(EquipementConcret.getEquipement(EquipementConcret.JETONVERT.toString()))
                && equipementsInv.contains(EquipementConcret.getEquipement(EquipementConcret.JETONROUGE.toString()))) {
            
            System.out.println("Vous avez réuni les trois jetons; Bravo !");
        }
    }
}
