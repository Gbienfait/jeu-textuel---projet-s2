/*
 * Copyright (C) 2018 Guillaume & Eloi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package projettp_bp;

/**
 *
 * @author eac, gbienfait
 * 
 * Classe héritée de la classe objet.
 */
public class Consommable extends Objet {
    int soins;

    /**
     * Constructeur d'un consommable.
     * 
     * @param soins
     * @param nom
     * @param prix 
     * 
     * Les paramètres nom & prix font référence à la classe mère Objet. Donc, par défaut, un consommable est un objet.
     */
    public Consommable(int soins, String nom, int prix) {
        super(nom, prix);
        this.soins = soins;
    }
    
    @Override
    public String toString(){
        return "- " + "\033[34m" + nom + "\033[00m"
                + "\n" + " soins : " + soins;
    }
}
