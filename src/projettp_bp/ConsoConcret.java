/*
 * Copyright (C) 2018 Guillaume & Eloi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package projettp_bp;

/**
 *
 * @author eac, gbienfait
 *
 * Enumération de consommables concrets
 */
public enum ConsoConcret  {
    BARRECHOCO(10, "Barre chocolatée",2),
    ANTIDOULEURS(25, "Anti-douleurs", 5),
    TROUSSESOINS(50, "Trousse de soins", 10);
    
    private int soins;
    private String nom;
    private int prix;
    
    /**
     * Constructeur d'un consommable conret (ConsoConcret)
     *
     * @param soins, L'objet restaurera des points de vie.
     * @param nom, un objet a un nom qui lui est propre.
     * @param prix, on associera un prix à un objet précis.
     */
    private ConsoConcret(int soins, String name, int prix){
        this.soins = soins;
        this.nom = name;
        this.prix = prix;
    }
    
    /**
     * Getter d'un Consommable.
     *
     * @param nom
     *
     * @return un objet de type Consommable à partir de son nom
     */
    public static Consommable getConso(String nom){
        for(ConsoConcret cC : ConsoConcret.values()){
            if(cC.toString().equals(nom)){
                return (new Consommable(cC.soins, cC.nom, cC.prix));
            }
        }
        return null;
    }
    
    /**
     * Méthoode publique toString() qui permet de convertir l'entrée de nom en
     * string.
     *
     * @return nom
     */
    @Override
    public String toString(){
        return nom;
    }
    
    
}
