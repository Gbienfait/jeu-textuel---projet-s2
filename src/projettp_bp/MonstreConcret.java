/*
 * Copyright (C) 2018 Guillaume & Eloi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package projettp_bp;

/**
 *
 * @author eac, gbienfait
 *
 * Enumération de monstres concrets.
 */
public enum MonstreConcret {
    GOULEF("Goule fragile", 20, 15, 20),
    RP("Reaper", 50, 30, 20),
    KRE("Kiore, rat géant", 100, 10, 40),
    KPA("Scorpion Kapiona", 80, 65, 50);

    private double defense;
    private double attaque;
    private String nom;
    private double vie;

    /**
     * Constructeur d'un monstre concret.
     *
     * @param nom, un monstre aura une appellation propre.
     * @param vie, nombre de points de vie d'un monstre concret.
     * @param attaque, stats d'attauqe d'un monstre concret.
     * @param defense, stats de défense d'un monstre concret.
     */
    MonstreConcret(String nom, double vie, double attaque, double defense) {
        this.nom = nom;
        this.vie = vie;
        this.attaque = attaque;
        this.defense = defense;
    }
    
    /**
     * @param nom, un monstre aura une appélation propre.
     * 
     * @return un Monstre à partie de son nom.
     */
    public static Monstre getMonstre(String nom){
        for(MonstreConcret mC : MonstreConcret.values()){
            if(mC.toString().equals(nom)){
                return (new Monstre(mC.nom, mC.vie, mC.attaque, mC.defense));
            }
        }
        return null;
    }
    
    /**
     * Getter du nom du monstre.
     *
     * @return nom
     */
    public String getNom(){
        return nom;
    }
}
