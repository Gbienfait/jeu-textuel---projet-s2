/*
 * Copyright (C) 2018 Guillaume & Eloi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package projettp_bp;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.TreeMap;

/**
 *
 * @author eac, gbienfait
 *
 * Classe Salle.
 */
public class Salle {

    /// Caractéristiques d'une salle
    private ArrayList<Objet> objetsPrésents = new ArrayList<>();
    private Monstre MonstrePrésent;
    private EnumMap<Directions, Boolean> dirDispo = new EnumMap<>(Directions.class);

    /// Tableaux des valeurs des énumérations associées, ils permettent par la suite de séléctionner aléatoirement une des valeurs
    private ConsoConcret[] cC = ConsoConcret.values();
    private EquipementConcret[] eC = EquipementConcret.values();

    /**
     * Constructeur d'une Salle
     *
     * @param filename, le nom du fichier dans lequel se trouve la définition de
     * la salle
     *
     */
    public Salle(String filename) {
        
        // Construction de la salle à partir du fichier .txt associé
        try {
            Scanner f = new Scanner(new File(filename));
            String mot = f.next();
            dirDispo.put(Directions.SUD, mot.equals("S"));
            mot = f.next();
            dirDispo.put(Directions.NORD, mot.equals("N"));
            mot = f.next();
            dirDispo.put(Directions.EST, mot.equals("E"));
            mot = f.next();
            dirDispo.put(Directions.OUEST, mot.equals("O"));
            if (f.hasNext()) {
                String type = f.next();
                boolean hostile = type.equals("H");
                String namePNJ = f.next();
                if (hostile) {
                    MonstrePrésent = MonstreConcret.getMonstre(namePNJ);
                } else {
                    MonstrePrésent = MonstreConcret.getMonstre(namePNJ);
                }
                if (MonstrePrésent == null) {
                    throw new PNJInexsistantException();
                }
            }
        } catch (NoSuchElementException e) {
            System.out.println("Il manque des éléments dans le fichier de définition de la salle " + filename);
            System.out.println(e.getMessage());
            System.exit(1);
        } catch (FileNotFoundException e) {
            System.out.println("Le fichier " + filename + " n'a pas été trouvé.");
        } catch (PNJInexsistantException ex) {
            System.out.println("Un des noms de PNJ donnée dans " + filename + " n'existe pas dans la liste des PNJ créables (= les enums associés).");
            System.exit(1);
        }

        // Ajout des objets dans la salle
        ajoutObjAléatoires();

        // Affichage d'un texte d'ambiance lorsque l'on change de salle
        afficheTxtAmbianceSalle();

        // Affichage d'un texte d'ambiance lorsque l'on entre dans une salle avec monstre
        afficheTxtPrésenceMstr();
    }

    /**
     * Affiche un texte d'ambiance si un monstre est présent dans la salle
     */
    private void afficheTxtPrésenceMstr() {
        if (MonstrePrésent != null) {
            Map<Integer, String> texteMonstre = new TreeMap<Integer, String>();
            texteMonstre.put(0, "Un(e) " + MonstrePrésent.nom + " semble dormir dans un coin de la pièce.");
            texteMonstre.put(1, "Un(e) " + MonstrePrésent.nom + " a l'air de ne même pas avoir remarqué votre présence.");
            texteMonstre.put(2, "Un(e) " + MonstrePrésent.nom + " vous regarde comme si vous étiez son repas...");
            int rd = (int) (Math.random() * texteMonstre.size());
            System.out.println(texteMonstre.get(rd));
        }
    }

    /**
     * Affiche un texte d'ambiance aléatoire
     */
    private void afficheTxtAmbianceSalle() {
        Map<Integer, String> texteAmbiance = new TreeMap<Integer, String>();
        texteAmbiance.put(0, "Vous pénétrez dans une pièce lugubre.");
        texteAmbiance.put(1, "Vous accédez à une nouvelle salle.");
        texteAmbiance.put(2, "Une nouvelle zone s'ouvre à vous.");
        int rd = (int) (Math.random() * texteAmbiance.size());
        System.out.println(texteAmbiance.get(rd));
    }

    /**
     * Ajoute de façon aléatoire des objets dans la salle
     */
    private void ajoutObjAléatoires() {
        int rd = (int) (Math.random() * 6); // Un nombre aléatoire entre 0 et 5
        int rd1 = (int) (Math.random() * cC.length); // Un nombre aléatoire entre 0 et le nombre de ConsoConcret
        int rd2 = (int) (Math.random() * eC.length); // Un nombre aléatoire entre 0 et le nombre d'EquipementConcret

        switch (rd) {
            case 0:
                objetsPrésents.add(ConsoConcret.getConso(cC[rd1].toString())); // Ajoute une conso tiré aléatoirement dans la salle
                break;
            case 1:
                objetsPrésents.add(EquipementConcret.getEquipement(eC[rd2].toString())); // Ajoute un equipement tiré aléatoirement dans la salle
                break;
            case 2:
                objetsPrésents.add(ConsoConcret.getConso(cC[rd].toString()));
                objetsPrésents.add(EquipementConcret.getEquipement(eC[rd].toString()));
                break;
            default:
                // La salle sera vide d'objets
                break;
        }
    }

    /**
     * @return false si la salle ne contient pas d'objets, true sinon.
     */
    public boolean objetPrésent() {
        return !objetsPrésents.isEmpty();
    }

    /**
     * Getter d'objet présent dans la salle courante.
     *
     * @return objetsPrésents
     */
    public ArrayList<Objet> getObjetsPrésents() {
        return objetsPrésents;
    }

    /**
     * Getter du monstre présent dans la salle courante.
     *
     * @return MonstrePrésent
     */
    public Monstre getMonstrePrésent() {
        return MonstrePrésent;
    }

    /**
     * Getter de la direction possible pour le déplacement du joueur.
     *
     * @return dirDispo
     */
    public EnumMap<Directions, Boolean> getDirDispo() {
        return dirDispo;
    }

}
