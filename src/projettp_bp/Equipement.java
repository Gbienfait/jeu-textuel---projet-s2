/*
 * Copyright (C) 2018 Guillaume & Eloi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package projettp_bp;

/**
 *
 * @author eac, gbienfait
 *
 * Classe Equipement qui hérite de la classe Objet.
 */
public class Equipement extends Objet {
    private final double attaque;
    private final double defense;
    private final TypeEquipement type;

    /**
     * Constructeur d'un équipement. Celui-ci améliorera les stats du joueur.
     *
     * @param attaque, correspond à un entier qui établira l'amélioration de
     *                  l'attaque.
     * @param defense, correspond à un entier qui établira l'amélioration de la
     *                  défense.
     * @param type, un objet sera d'un type précis (casque, plastron, etc.)
     * @param nom, un objet a un nom qui lui est propre.
     * @param prix, on associera un prix à un objet précis.
     *
     * Les paramètres nom & prix font référence à ceux du constructeur de la
     * classe mère, Objet.
     */
    public Equipement(double attaque, double defense, TypeEquipement type, String nom, int prix) {
        super(nom, prix);
        this.attaque = attaque;
        this.defense = defense;
        this.type = type;
    }

    /**
     * Getter des stats de l'attaque.
     * 
     * @return attaque
     */
    public double getAttaque() {
        return attaque;
    }

    /**
     * Getter des stats de la défense.
     * 
     * @return defense
     */
    public double getDefense() {
        return defense;
    }

    /**
     * Getter du type d'équipement.
     * 
     * @return type
     */
    public TypeEquipement getType() {
        return type;
    }
    
    /**
     * @return true si l'equipement est un objet de quête
     */
    @Override
    public boolean EstObjetDeQuete(){
        return type == TypeEquipement.OBJETDEQUETE;
    }
    
    /**
     * @return true s'il s'agit d'un équipement
     */
    @Override
    public boolean EstEquipement(){
        return true;
    }
    
    @Override
    public String toString(){
        return "- " + "\033[34m" + nom + "\033[00m"
                + "\n" + " attaque : " + attaque + ", "
                + "defense : " + defense + ", "
                + "type : " + type.toString();
    }
}
