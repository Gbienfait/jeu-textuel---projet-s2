/*
 * Copyright (C) 2018 Guillaume & Eloi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package projettp_bp;

import java.util.Scanner;

/**
 *
 * @author eac, gbienfait
 *
 * Classe gérant les textes d'affichage et les différentes intéractions
 * possibles.
 */
public class TextDialog {

    int niveauVerticale = 1;
    int niveauVertMax = 3;
    int niveauHorizontale = 1;
    int niveauHorMax = 2;
    Salle salle = new Salle("Lab11.txt");
    static Joueur joueur = new Joueur();

    public TextDialog() {
        String commande;
        Scanner in = new Scanner(System.in);
        while (in.hasNextLine()) {
            Scanner s = new Scanner(in.nextLine());
            while (s.hasNext()) {
                commande = s.next();

                // Switch pour les différentes actions réalisables par le joueur (déplacement, inspection etc.)
                switch (commande) {
                    case "quit":
                        executeQuit();
                        break;
                    case "help":
                        executeHelp();
                        break;
                    case "monter":
                        monter();
                        break;
                    case "descendre":
                        descendre();
                        break;
                    case "aDroite":
                        aDroite();
                        break;
                    case "aGauche":
                        aGauche();
                        break;
                    case "inspecter":
                        joueur.inspecter(salle);
                        break;
                    case "voirInventaire":
                        joueur.voirInventaire();
                        break;
                    case "utiliser":
                        joueur.utiliser();
                        break;
                    case "ramasser":
                        ramasser();
                        break;
                    case "attaquer":
                        attaquer();
                        break;
                    case "statut":
                        joueur.afficheStatut();
                        break;
                    default:
                        System.out.println("Commande non reconnue : " + commande + "\n"
                                + "Tapez " + "\033[34m" + "help" + "\033[00m" + " pour afficher les commandes disponibles");
                        break;
                }
            }
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        System.out.println("2057. Caroline du Sud, Etats-Unis. Vous vous réveillez étendu sur des débris. D’étranges bruits résonnent dans le bâtiment.\n"
                + "Des morceaux de murs et de plafonds s'effondrent. Votre corps est comme paralysé. Vous regardez autour de vous. Le chaos est omniprésent.\n"
                + "La plus grosse catastrophe naturelle a frappée la Terre. Une météorite de neuf kilomètres de diamètres se déplaçant à une vitesse de 72 km/s a percutée notre planète\n"
                + "et en a bouleversée l’équilibre. Typhons, tsunamis, séismes, éruptions volcaniques … Gaïa se réveille. \n"
                + "Des villes entières ont été rasées, des pays rayés de la carte et des continents explosés par la violence des évènements. \n"
                + "La race humaine a été éradiquée. Enfin presque …\n"
                + "Vous incarnez Talion, un jeune diplômé d’IUT. Sportif, intelligent, vous êtes le dernier Homme sur Terre. \n"
                + "L’écosystème a été infecté par tous les déchets nucléaires déplacés par vents et marrés. \n"
                + "Votre réveil a lieu plusieurs mois après la collision. La nature a repris le dessus. \n"
                + "Vous tentez de chercher le moindre signe de vie, la moindre personne capable de vous aider dans ce désordre planétaire.");
        Thread.sleep(5000); // L'exécution du programme se stoppe pendant un temps donné (en millisecondes) avant de reprendre.
        new TextDialog();
    }

    // Méthode pour quitter le jeu en cours.
    private void executeQuit() {
        System.exit(0);
    }

    // Affichage des commandes disponibles dans le jeu pour aider l'utilisateur.
    private void executeHelp() {
        System.out.println("Les commandes disponilbes :");
        System.out.println("\033[34m" + "   quit" + "\033[00m" + " : Stoppe le programme");
        System.out.println("\033[34m" + "   help" + "\033[00m" + " : Affiche l'aide");
        System.out.println("\033[34m" + "   monter, descendre, aGauche, aDroite" + "\033[00m" + " : Permet de se déplacer dans les différentes salles");
        System.out.println("\033[34m" + "   inspecter" + "\033[00m" + " : Liste les objets présents dans la salle");
        System.out.println("\033[34m" + "   ramasser" + "\033[00m" + " : Stocke l'objet séléctionné dans votre inventaire");
        System.out.println("\033[34m" + "   voirInventaire" + "\033[00m" + " : Liste les objets présents dans votre inventaire");
        System.out.println("\033[34m" + "   utiliser" + "\033[00m" + " : Consommer/Equiper un objet présent dans votre inventaire");
        System.out.println("\033[34m" + "   attaquer" + "\033[00m" + " : Vous attaquez le monstre dans la salle, gare à vos points de vie !");
        System.out.println("\033[34m" + "   statut" + "\033[00m" + " : Vous donne vos statistiques et votre équipement actuel");
    }

    ////////// Déplacement possibles par l'utilisateur //////////
    private void monter() {
        if (salle.getDirDispo().get(Directions.NORD) && niveauVerticale - 1 >= 1) {
            niveauVerticale--;
            salle = new Salle("Lab" + niveauVerticale + "" + niveauHorizontale + ".txt");
            System.out.println("Salle " + "\033[35m" + niveauVerticale + "-" + niveauHorizontale + "\033[00m");
        } else {
            System.out.println("Vous ne pouvez pas vous déplacer dans cette pièce");
        }
    }

    private void descendre() {
        if (salle.getDirDispo().get(Directions.SUD) && niveauVerticale + 1 <= niveauVertMax) {
            niveauVerticale++;
            salle = new Salle("Lab" + niveauVerticale + "" + niveauHorizontale + ".txt");
            System.out.println("Salle " + "\033[35m" + niveauVerticale + "-" + niveauHorizontale + "\033[00m");
        } else {
            System.out.println("Vous ne pouvez pas vous déplacer dans cette pièce");
        }
    }

    private void aGauche() {
        if (salle.getDirDispo().get(Directions.OUEST) && niveauHorizontale - 1 >= 1) {
            niveauHorizontale--;
            salle = new Salle("Lab" + niveauVerticale + "" + niveauHorizontale + ".txt");
            System.out.println("Salle " + "\033[35m" + niveauVerticale + "-" + niveauHorizontale + "\033[00m");
        } else {
            System.out.println("Vous ne pouvez pas vous déplacer dans cette pièce");
        }
    }

    private void aDroite() {
        if (salle.getDirDispo().get(Directions.EST) && niveauHorizontale + 1 <= niveauHorMax) {
            niveauHorizontale++;
            salle = new Salle("Lab" + niveauVerticale + "" + niveauHorizontale + ".txt");
            System.out.println("Salle " + "\033[35m" + niveauVerticale + "-" + niveauHorizontale + "\033[00m");
        } else {
            System.out.println("Vous ne pouvez pas vous déplacer dans cette pièce");
        }
    }

    // Méthode ramasser() pour permettre au joueur d'ajouter un objet souhaité, présent dans la salle courante, à son inventaire.
    private void ramasser() {
        if (salle.objetPrésent()) {
            joueur.ramasser(salle);
        } else {
            System.out.println("\033[00m" + "Il n'y a pas d'objets dans la salle.");
        }
    }

    // Lance l'attaque sur le Monstre présent dans la salle.
    private void attaquer() {
        if (salle.getMonstrePrésent() != null) {
            joueur.attaquer(salle.getMonstrePrésent());
            salle.getMonstrePrésent().abattu();
        } else {
            System.out.println("Il n'y a pas de monstre à attaquer dans cette salle.");
        }
    }
}
