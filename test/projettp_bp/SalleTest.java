/*
 * Copyright (C) 2018 Guillaume & Eloi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package projettp_bp;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Guillaume
 */
public class SalleTest {
    
    public SalleTest() {
    }

    Salle salle = new Salle("Lab11.txt");
    /**
     * Test qui vérifie qu'il y a bien un monstre dans la salle 1-1
     */
    @Test
    public void testMonstrePrésent() {
        System.out.println("objetPr\u00e9sent");
        boolean expResult = true;
        boolean result = salle.getMonstrePrésent() != null;
        assertEquals(expResult, result);
    }
}
